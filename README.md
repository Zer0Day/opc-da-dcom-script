# OPC DCOM Script

#### 介绍
自动配置DCOM的脚本。

通过Powershell脚本调用微软的DCOMPerm工具实现。

如果想获取DCOMPerm的源码，请参照[这里](https://github.com/Microsoft/Windows-classic-samples/tree/main/Samples/Win7Samples/com/fundamentals/dcom/dcomperm)。

本仓库的DCOMPerm是我自己编译的，如果觉得不放心可以使用微软的源代码自行编译。


#### 使用说明

1.  opc客户端需要安装opc运行库，可以去opc基金会官网下载。
2.  克隆本仓库或者手动下载全部的文件。
3.  以管理员身份运行DCOM-Config.ps1脚本，并根据脚本提示进行操作。
4.  如果电脑Powershell脚本执行策略是Restricted会导致脚本无法执行。请参考[这里](https://learn.microsoft.com/zh-cn/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.5)

### 测试情况

已经在以下系统做过测试：

 	1. windows 10 专业版、windows 10 企业版
 	2. windows server 2016、window server2019、windows server2022

==可能存在问题的系统：==

​	1. windows 7。可能在某些奇异的耦合下导致windows 7 的防火墙故障。在windows 7 系统下使用此脚本请考虑手动配置防火墙。
