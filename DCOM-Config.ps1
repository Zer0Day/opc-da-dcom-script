# PowerShell Script for Configuring OPC Server and Client

Write-Host "*********************************重要提示*************************************************"
Write-Host " "
Write-Host "  如果要配置 OPC 服务器，请提前准备好 OPC Server 的可执行文件路径和 Server 的 CLSID。"
Write-Host "  如果配置 OPC 客户端，请先安装 OPC 运行库。"
Write-Host "  "
Write-Host "******************************************************************************************"

$choice = Read-Host "请选择配置服务器端(s)/客户端(c)"
while ($choice -ne 's' -and $choice -ne 'c') {
    $choice = Read-Host "输入无效，请重新输入 (c/s)"
}

if ($choice -eq 's') {
    Write-Host "开始配置 OPC 服务器端"
    Write-Host "OPC Server 可执行文件路径不需要引号包裹，即使路径包含空格。" -ForegroundColor Cyan 
    $OpcServer = Read-Host "OPC Server 可执行文件路径"
    Write-Host "OPC Server CLSID要包含花括号，例如{FF9F1A4B-E79A-43E6-B79E-166F5ACC2944}。" -ForegroundColor Cyan 
    $OpcServerCLSID = Read-Host "OPC Server CLSID"
    while ([string]::IsNullOrEmpty($opcserverCLSID)) {
        $OpcServerCLSID = Read-Host "OPC Server CLSID"
    }
    $ServerIdentity = Read-Host "请选择opc server的标识:交互式用户(i)/启动用户(l)/指定用户(u)/不做修改(n),默认值为n"
    if ($ServerIdentity -eq "") { $ServerIdentity = "n" }
    # while ($ServerIdentity -ne 'i' -and $ServerIdentity -ne 'l' -and $ServerIdentity -ne 'u' -and $ServerIdentity -ne 'n') {
    #     $ServerIdentity = Read-Host "请选择opc server的标识:交互式用户(i)/启动用户(l)/指定用户(u)/不做修改(n)"
    # }
    if ($ServerIdentity -eq 'u') {
        #指定用户时需要输入用户名和密码
        $OpcUser = Read-Host "OPC Server 运行账户"
        while ($OpcUser -eq "") {
            $OpcUser = Read-Host "OPC Server 运行账户"
        }
        $OpcUserPassword = Read-Host "OPC Server 运行账户密码"
        while ($OpcUserPassword -eq "") {
            $OpcUserPassword = Read-Host "OPC Server 运行账户密码"
        }
    }
}
else {
    Write-Host "开始配置 OPC 客户端"
    $OpcUser = Read-Host "OPC Server 运行账户"
    while ($OpcUser -eq "") {
        $OpcUser = Read-Host "OPC Server 运行账户"
    }
}

# 设置 OPC Enum 路径和用户
$opcenum = "C:\Windows\SysWOW64\opcenum.exe"
Write-Host "OPC Enum 路径默认为:$opcenum。"  -ForegroundColor Cyan 
$temp = Read-Host "请输入opc enum路径或者回车使用默认值"
if ($temp -ne "") { $opcenum = $temp }


# 添加防火墙规则
Write-Host "添加防火墙规则..."
# 135端口例外 入站
$existingRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq "tcp135" }
if (-not $existingRule) {
    $result = New-NetFirewallRule -DisplayName "tcp135" -Direction Inbound -Protocol TCP -LocalPort 135 -Action Allow
    if ($LASTEXITCODE -ne 0) {
        Write-Host "错误: $result" -ForegroundColor Red
    }
}
else {
    Write-Host "规则已存在:tcp135" -ForegroundColor Yellow
}

$existingRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq "udp135" }
if (-not $existingRule) {
    $result = New-NetFirewallRule -DisplayName "udp135" -Direction Inbound -Protocol UDP -LocalPort 135 -Action Allow
    if ($LASTEXITCODE -ne 0) {
        Write-Host "错误: $result" -ForegroundColor Red
    }
}
else {
    Write-Host "规则已存在:udp135" -ForegroundColor Yellow
}

#opc server添加到防火墙例外 双向
if ($choice -eq 's') {
    $existingRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq "opcserver_Inbound" }
    if (-not $existingRule) {
        $result = New-NetFirewallRule -DisplayName "opcserver_Inbound" -Direction Inbound -Program $opcserver -RemoteAddress Any -Action Allow
        if ($LASTEXITCODE -ne 0) {
            Write-Host "错误: $result" -ForegroundColor Red
        }
    }
    else {
        Write-Host "规则已存在:opcserver_Inbound" -ForegroundColor Yellow
    }

    $existingRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq "opcserver_Outbound" }
    if (-not $existingRule) {
        $result = New-NetFirewallRule -DisplayName "opcserver_Outbound" -Direction Outbound -Program $opcserver -RemoteAddress Any -Action Allow
        if ($LASTEXITCODE -ne 0) {
            Write-Host "错误: $result" -ForegroundColor Red
        }
    }
    else {
        Write-Host "规则已存在:opcserver_Outbound" -ForegroundColor Yellow
    }
}

#opc enum添加到防火墙例外 双向
$existingRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq "opcenum_Inbound" }
if (-not $existingRule) {
    $result = New-NetFirewallRule -DisplayName "opcenum_Inbound" -Direction Inbound -Program $opcenum -RemoteAddress Any -Action Allow
    if ($LASTEXITCODE -ne 0) {
        Write-Host "错误: $result" -ForegroundColor Red
    }
}
else {
    Write-Host "规则已存在:opcenum_Inbound" -ForegroundColor Yellow
}

$existingRule = Get-NetFirewallRule | Where-Object { $_.DisplayName -eq "opcenum_Outbound" }
if (-not $existingRule) {
    $result = New-NetFirewallRule -DisplayName "opcenum_Outbound" -Direction Outbound -Program $opcenum -RemoteAddress Any -Action Allow
    if ($LASTEXITCODE -ne 0) {
        Write-Host "错误: $result" -ForegroundColor Red
    }
}
else {
    Write-Host "规则已存在:opcenum_Outbound" -ForegroundColor Yellow
}

# 配置 DCOM 权限

Write-Host "DCOM访问权限-编辑默认值"  
.\DComPerm.exe -da set "Distributed COM Users" permit level:r, l  
.\DComPerm.exe -da set "Anonymous Logon" permit level:r, l  
.\DComPerm.exe -da set "Everyone" permit level:r, l  
.\DComPerm.exe -da set "Interactive" permit level:r, l  
.\DComPerm.exe -da set "System" permit level:r, l  
.\DComPerm.exe -da set "SELF" permit level:r, l  
.\DComPerm.exe -da set "Administrators" permit level:r, l  
if (![string]::IsNullOrEmpty($opcuser)) {
    .\DComPerm.exe -da set %opcuser% permit level:r, l  
}

Write-Host "DCOM访问权限-编辑限制"  
.\DComPerm.exe -ma set "Distributed COM Users" permit level:r, l  
.\DComPerm.exe -ma set "Anonymous Logon" permit level:r, l  
.\DComPerm.exe -ma set "Everyone" permit level:r, l  
.\DComPerm.exe -ma set "Interactive" permit level:r, l  
.\DComPerm.exe -ma set "System" permit level:r, l 
.\DComPerm.exe -ma set "Administrators" permit level:r, l  
if (![string]::IsNullOrEmpty($opcuser)) {
    .\DComPerm.exe -ma set "%opcuser%" permit level:r, l   
}

Write-Host "DCOM启动和激活权限-编辑默认值"
.\DComPerm.exe -dl set "Distributed COM Users" permit level:rl, ll, la, ra  
.\DComPerm.exe -dl set "Anonymous Logon" permit level:rl, ll, la, ra  
.\DComPerm.exe -dl set "Everyone" permit level:rl, ll, la, ra  
.\DComPerm.exe -dl set "Interactive" permit level:rl, ll, la, ra  
.\DComPerm.exe -dl set "System" permit level:rl, ll, la, ra  
.\DComPerm.exe -dl set "Administrators" permit level:rl, ll, la, ra
if (![string]::IsNullOrEmpty($opcuser)) {
    .\DComPerm.exe -dl set "$opcuser"  permit level:rl, ll, la, ra
}

Write-Host "DCOM启动和激活权限限制-编辑限制"
.\DComPerm.exe -ml set "Distributed COM Users" permit level:rl, ll, la, ra  
.\DComPerm.exe -ml set "Anonymous Logon" permit level:rl, ll, la, ra  
.\DComPerm.exe -ml set "Everyone" permit level:rl, ll, la, ra  
.\DComPerm.exe -ml set "Interactive" permit level:rl, ll, la, ra  
.\DComPerm.exe -ml set "System" permit level:rl, ll, la, ra  
.\DComPerm.exe -ml set "Administrators" permit level:rl, ll, la, ra  
if (![string]::IsNullOrEmpty($opcuser)) {
    .\DComPerm.exe -ml set "$opcuser" permit level:rl, ll, la, ra  
}

Write-Host "配置 OPC enum"
# 不加引号的GUID在powershell会导致错误，CMD中就没有问题。
.\DComPerm.exe -al "{13486D44-4821-11D2-A494-3CB306C10000}" Default 
.\DComPerm.exe -aa "{13486D44-4821-11D2-A494-3CB306C10000}" Default 

if ($choice -eq 's') {
    Write-Host "配置 OPC Server"
    .\DComPerm.exe -al "$OpcServerCLSID" Default 
    .\DComPerm.exe -aa "$OpcServerCLSID" Default  

    # 配置标识
    if ($ServerIdentity -eq 'i') {
        .\DComPerm.exe -runas "$OpcServerCLSID" "INTERACTIVE USER"
    }
    elseif ($ServerIdentity -eq 'l') {
        .\DComPerm.exe -runas "$OpcServerCLSID" "LAUNCHING USER"
    }
    elseif ($ServerIdentity -eq 'u') {
        .\DComPerm.exe -runas "$OpcServerCLSID" $OpcUser $OpcUserPassword
    }
    else {
        Write-Host "opc server 标识未做修改！" -ForegroundColor Yellow
    }
}

# 更新注册表
Write-Host "更新 DCOM 相关注册表..."
$result = reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Ole" /v "EnableDCOM" /t REG_SZ /d "Y" /f  2>&1
if ($LASTEXITCODE -ne 0) {
    Write-Host "错误: $result" -ForegroundColor Red
}
$result = reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Ole" /v "LegacyAuthenticationLevel" /t REG_DWORD /d 2 /f  2>&1
if ($LASTEXITCODE -ne 0) {
    Write-Host "错误: $result" -ForegroundColor Red
}
$result = reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Ole" /v "LegacyImpersonationLevel" /t REG_DWORD /d 2 /f  2>&1
if ($LASTEXITCODE -ne 0) {
    Write-Host "错误: $result" -ForegroundColor Red
}
$result = reg add "HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\Lsa" /v "forceguest" /t REG_DWORD /d 0 /f  2>&1
if ($LASTEXITCODE -ne 0) {
    Write-Host "错误: $result" -ForegroundColor Red
}

# 关闭防火墙（可选）
$fw = Read-Host "是否关闭防火墙 (y/n, 默认 y)"
if ($fw -eq "" -or $fw -eq "y") {
    Write-Host "关闭防火墙"
    $result = netsh advfirewall set allprofiles state off
    if ($LASTEXITCODE -ne 0) {
        Write-Host "错误: $result" -ForegroundColor Red
    }
}

# 启用 DCOM 日志（可选）
$enlog = Read-Host "是否启用 DCOM 日志 (y/n, 默认 y)"
if ($enlog -eq "" -or $enlog -eq "y") {
    Write-Host "启用DCOM日志"
    $result = reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Ole" /v "ActivationFailureLoggingLevel" /t REG_DWORD /d 1 /f  2>&1
    if ($LASTEXITCODE -ne 0) {
        Write-Host "错误: $result" -ForegroundColor Red
    }
    $result = reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Ole" /v "CallFailureLoggingLevel" /t REG_DWORD /d 1 /f  2>&1
    if ($LASTEXITCODE -ne 0) {
        Write-Host "错误: $result" -ForegroundColor Red
    }
    $result = reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Ole" /v "InvalidSecurityDescriptorLoggingLevel" /t REG_DWORD /d 1 /f  2>&1
    if ($LASTEXITCODE -ne 0) {
        Write-Host "错误: $result" -ForegroundColor Red
    }
}

Write-Host "配置完成，按任意键退出..."
Read-Host